const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const http = require('http');
// const dotenv = require('dotenv');
const apiRoutes = require('./routes');
const socketRoutes = require('./socketRoutes');
const socketIo = require('./config/socketConfig');





// dotenv.config();

// SERVER SETUP
const app = express();
const httpServer = http.createServer(app);
const io = socketIo(httpServer);


// MONGODB CONNECTION
const mongoURI = process.env.MONGO_URI


// Or using promises
mongoose.connect(mongoURI, {}).then(
  () => {
    /** ready to use. The `mongoose.connect()` promise resolves to mongoose instance. */
    console.log('MongoDB Connected successfully');
  },
  (err) => {
    /** handle initial connection error */
    console.log("MongoDB Error: ", err);
  }
);


const socketIoRoomMap = new Map();




io.on("connection", (socket) => {
  // Create room and join the room
  socket.on('join-room-from-client', async (roomId) => {
    socketIoRoomMap.set(roomId, { user1: true, user2: false });
    // console.log("join room - ", socketIoRoomMap);
    socket.join(roomId);
  });

  socket.on(
    'connect-user-from-client',
    async (roomId) => {
      const joinedUser = socketIoRoomMap.get(roomId)
      // console.log("Connect room - ",socketIoRoomMap);
      // console.log("match roomId - ", roomId);
      console.log("join user - ", joinedUser);
      if (!joinedUser) {
        console.log("room not found or no user in the room");
        socketIoRoomMap.set(roomId, { user1: true, user2: false });
        return socket.join(roomId);
      } else if (joinedUser.user1 && joinedUser.user2) {
        console.log("No more than 2 user in a room");
        return;
      } else if (!joinedUser.user1) {
        socketIoRoomMap.set(roomId, { user1: true, user2: false });
        console.log("Join room");
        return socket.join(roomId);
      }
      console.log("connected ------- ");
      socketIoRoomMap.set(roomId, { user1: true, user2: true });
      socket.join(roomId);
      io.to(roomId).emit('estanblish-connection-from-server');
    }
  );

  socket.on('disconnect', () => {
    console.log('disconnect - ', socket.rooms); // the Set contains at least the socket ID
  });

  socket.on(
    'send-file-offer-signal',
    ({
      roomId,
      signal,
      uploadedFilename,
    }) => {
      socket
        .to(roomId)
        .emit('getting-offer-signal', { signal, uploadedFilename });
    }
  );

  socket.on('clear-send-file', (roomId) => {
    socket.to(roomId).emit('cancel-send-file')
  });

  socket.on(
    'answer-the-offer',
    ({ signal, roomId }) => {
      console.log('Signal should be answer - ', signal);
      socket.to(roomId).emit('offer-is-been-accepted', signal);
    }
  );
});

// MIDDLEWARE
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/', apiRoutes);



// RUNNING SERVER
const port = process.env.PORT || 9000;
httpServer.listen(port, () => console.log('Server is running on : ' + port));

// module.exports = { io };
