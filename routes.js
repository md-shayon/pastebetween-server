const { Router } = require( 'express');
const Text = require( './models/Text');
const Url = require( './models/Url');
const { nanoid, customAlphabet } = require( 'nanoid');

const router = Router();

// ROUTES - https://www.youtube.com/watch?v=4WvX9dBjiJo
router.get('/r', (req, res) => {
  res.status(200).json({ msg: 'Testing mode' });
});

router.post('/r/textlink', async (req, res) => {
  try {
    // console.log(req.body);

    const { linkText } = req.body;

    // Check link text

    // Create new
    const newTextLink = new Text({
      desc: linkText,
      sender: 'random',
    });
    // Save and  Get text id
    const savedLinkText = await newTextLink.save();
    // Generate link
    const generatedLink = 'random-generated-link';
    console.log(savedLinkText);

    return res.status(201).json({
      msg: 'data saved successfully',
      textId: savedLinkText._id,
      generatedLink,
    });
  } catch (error) {
    console.log(error);
  }
  res.status(500).json({ msg: 'Server error' });
});

router.get('/r/textlink/:textlinkId', async (req, res) => {
  try {
    // console.log(req.body);

    const { textlinkId } = req.params;

    // Create new
    const findText = await Text.findById(textlinkId);

    if (!findText) return res.status(404).json({ msg: 'Text not found' });

    // Create a date object for the given date
    // @ts-ignore
    const givenDate = new Date(findText.createdAt);

    // Get the current date
    const currentDate = new Date();

    // Calculate the difference between the two dates in milliseconds
    // @ts-ignore
    const diffInMs = currentDate - givenDate;

    // Calculate the number of milliseconds in 7 days
    const sevenDaysInMs = 7 * 24 * 60 * 60 * 1000;

    // Compare the difference with seven days
    if (diffInMs > sevenDaysInMs) {
      console.log('The given date is over 7 days old.');
      return res.status(403).json({ msg: 'Text url is expired', findText });
    } else {
      console.log('The given date is not over 7 days old.');
      return res.status(201).json({ msg: 'data saved successfully', findText });
    }
  } catch (error) {
    console.log(error);
  }
  res.status(500).json({ msg: 'Server error' });
});

router.post('/r/shorturl', async (req, res) => {
  if (!req.body.linkText)
    return res.status(400).json({ msg: 'Url is required' });
  const urlParam = req.body.linkText;

  // const nanoidAlphabet = await customAlphabet(urlParam, 10)
  // const newId = nanoidAlphabet(8);
  const newId = await nanoid(8).toString();

  // console.log({newId});

  const newUrl = new Url({
    shortId: newId,
    redirectURL: urlParam,
    visitHistory: [],
  });
  const savedUrl = await newUrl.save();
  res.status(201).json({ msg: 'url has been created successfully', savedUrl });
});

// Redirect
router.get('/r/:shortId', async (req, res) => {
  try {
    const { shortId } = req.params;
    const updateUrl = await Url.findOneAndUpdate(
      { shortId },
      {
        $push: {
          visitHistory: {
            timestamp: Date.now(),
          },
        },
      }
    );
    // return res.status(202).json({msg: 'Redirect success', redirectURL: updateUrl?.redirectURL})
    if (!updateUrl) return res.status(404).json({ msg: 'Not found' });
    return res.redirect(updateUrl.redirectURL);
  } catch (error) {
    console.log(error);
  }
  res.status(500).json({ msg: 'Server error' });
});

router.get(
  '/r/shorturl/history/:shortId',
  async (req, res) => {
    try {
      const { shortId } = req.params;
      const foundUrl = await Url.findOne({ shortId });
      // console.log(foundUrl);

      return res
        .status(200)
        .json({ msg: 'Found url', totalClicks: foundUrl?.visitHistory.length });
    } catch (error) {
      console.log(error);
    }
    res.status(500).json({ msg: 'Server error' });
  }
);

module.exports =  router;
