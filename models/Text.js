const mongoose = require('mongoose');



const textSchema = new mongoose.Schema({
    desc: {
        type: String,
        required: true
    },
    sender: String
},  { timestamps: true });


module.exports = mongoose.model('Text', textSchema);