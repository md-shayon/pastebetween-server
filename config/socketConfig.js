const { Server } =require('socket.io');

module.exports =  (httpServer)=> new Server(httpServer, {
    cors: {
      origin: process.env.FRONTEND_URL,
      allowedHeaders: ['my-custom-header'],
      credentials: true,
    },
    
    transports: ["websocket"]
  });
  